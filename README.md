**M I R O I R  L I Q U I D E**<br>
: :  Bonjour Monde<br>
: :  Université de Nîmes<br>
: :  février 2022<br>

————————————<br>
**S U J E T**

En travaillant sur le choix d'images et/ou de typographie passé par la moulinette de Liquid Rescale, les étudiants vont collectivement réaliser une édition de "De l'autre côté du Miroir" de Lewis Caroll. A partir d'un mise en page de base, chaque étudiant devra s'approprier un fragment du texte et le mettre en forme en cohérence sur 16 pages format A7.

————————————<br>
**M É T H O D O L O G I E**

L'intégralité de "De l'Autre côté du miroir" de Lewis Caroll a été divisé afin que chaque élève mette en page et illustre un tier de chapitre dans un livret de 16 pages qui tient sur une page A4 recto-verso. Les illustrations seront réalisé grâce à Liquid Rescale et le texte mis en forme avec Paged-Js. 
Chaque groupe de trois étudiants travaillant sur le même chapitre créera une couverture qui sera imprimé en risographie deux-couleurs. Les trois cahiers seront ensuite relié ainsi que la couverture pour créer un petit livre par chapitre. De notre côté nous relieront un version de l'intégralité du texte qui fera 420 pages.<br>
————————————<br>
**C A L E N D R I E R**<br>

Mercredi 3 février
<br>— MATIN :
<br>- présentation de Bonjour Monde
<br>- Présentation de travaux graphiques impliquant des déformations et de livres remarquables
<br>— APRÈS-MIDI :
<br>- exercice autour de Liquid Rescale sur Gimp à partir d'images puis de typographie
<br>- introduction du sujet

<br>Mercredi 9 février
<br>— MATIN :
<br>- Travail graphique en autonomie
<br>— APRÈS-MIDI :
<br>- impressions et début d’accrochage pour regard collectif
<br>- poursuite des recherches graphiques
<br>- rendez-vous en groupes

<br>Lundi 14 février
<br>— MATIN :
<br>- Rendez-vous individuel et débugage
<br>— APRÈS-MIDI :
<br>-rendez-vous individuels et selection des couvertures

<br>Mardi 15 février
<br>— MATIN : 
<br>- impression des intérieurs et des couvertures 
<br>— APRÈS-MIDI :
<br>- Reliure


————————————<br>
**O U T I L S**<br>
• [GIMP](https://www.gimp.org/downloads/)<br>
• [Liqui Rescale](http://liquidrescale.wikidot.com/)<br>
• [Paged.JS](https://pagedjs.org/)<br>

————————————<br>
**L I E N S**
<br>• <a href="http://dansmapetiteroulotte.eklablog.fr/diy-fabriquer-un-livre-avec-une-reliure-a125635298">Une page sur la couture en reliure</a>
<br>• <a href="https://fr.wikipedia.org/wiki/Seam_carving">La page Wikipédia du Liqui Rescale


